/*
** EPITECH PROJECT, 2019
** workshop
** File description:
** A sample function that you can test!
*/

int my_function(int a)
{
    if (0 == a) {
        return -1;
    }
    return a;
}
